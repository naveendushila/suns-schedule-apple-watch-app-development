//
//  changeCity.swift
//  Sun Schedule WatchKit Extension
//
//  Created by Naveen Dushila on 2019-03-06.
//  Copyright © 2019 Naveen Dushila. All rights reserved.
//

import UIKit
import WatchKit
import Foundation

class changeCity: WKInterfaceController {
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
}
